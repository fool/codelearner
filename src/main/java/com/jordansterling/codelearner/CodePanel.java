package com.jordansterling.codelearner;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Jordan Sterling
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import java.awt.*;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.io.File;
import java.util.regex.*;
import javax.swing.*;

/**
 * The main display in the window
 */
public class CodePanel extends JPanel
{
    public final static String PRODUCE_FILE = "codes.txt";
    public final static String CODE_REGEX = "^([0-9]+)\\s+(.+)$";
    private final Vector<Produce> produce;
    private Produce current;

    private final StatsWindow statsWindow;
    private final StatsPanel statsPanel;

    private final JLabel questionLabel;
    private final JLabel resultLabel;

    public CodePanel(final CodeLearner codeLearner)
    {
        final GroceryCodeTextField groceryCodeTextField = new GroceryCodeTextField(this);
        setLayout(new BorderLayout());

        produce = new Vector<Produce>();
        loadProduceCodes(produce);
        statsWindow = new StatsWindow(codeLearner, produce);

        statsPanel = new StatsPanel(this);

        final JPanel groceryCodeContainer = new JPanel();
        groceryCodeContainer.add(groceryCodeTextField);

        final Font font = new Font("TimesRoman", Font.PLAIN, 20);
        questionLabel = new JLabel();
        questionLabel.setFont(font);
        resultLabel = new JLabel();
        resultLabel.setFont(font);

        final JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(2, 1));
        centerPanel.add(questionLabel);
        centerPanel.add(resultLabel);


        add(statsPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);
        add(groceryCodeContainer, BorderLayout.SOUTH);

        current = getNextProduce();
        questionLabel.setText(current.name);
        groceryCodeTextField.requestFocus();
    }

    /**
     * Reads the codes.txt file and loads all the produce.
     */
    private void loadProduceCodes(final Vector<Produce> container) {
        JTextPane errors = new JTextPane();
        final Scanner fileReader;
        try {
            /* start with user override file */
            File codesFile = new File(PRODUCE_FILE);
            if (codesFile.exists()) {
                fileReader = new Scanner(codesFile);
            } else {
                /* fall back to jar file, this should always exist but if not the catch will handle it */
                final String embeddedCodesFile = String.format("%sresources%s%s", File.separator, File.separator, PRODUCE_FILE);
                InputStream resource = getClass().getResourceAsStream(embeddedCodesFile);
                fileReader = new Scanner(resource);
            }

        }
        catch (Exception e) {
            System.out.println(String.format("ERROR MESSAGE: %s %s", e.getClass().getName(), e.getMessage()));
            e.printStackTrace();
            errors.setText(e.getMessage());
            return;
        }

        int count = 0;
        while (fileReader.hasNextLine()) {
            final String line = fileReader.nextLine();
            count++;
            final Option<Produce> produce = parseProduceCode(line);
            if (produce.isDefined()) {
                container.add(produce.get());
            } else {
                errors.setText(String.format("%s\nMalformed entry in codes file on line %d: %s", errors.getText(), count, line));
            }
        }

        int errorTextLength = errors.getText().length();
        if (errorTextLength > 0) {
            errors.setCaretPosition(errorTextLength);
            add(new JScrollPane(errors), BorderLayout.EAST);
        }
    }

    /**
     * Parses a single line in the codes.txt file
     */
    protected Option<Produce> parseProduceCode(final String line) {
        final Matcher matcher = Pattern.compile(CODE_REGEX).matcher(line);
        Produce produce = null;

        if (matcher.find()) {
            if (matcher.groupCount() == 2) {
                final String number = matcher.group(1);
                final String name = matcher.group(2);
                produce = new Produce(name, number);
            }
        }
        return new Option<Produce>(produce);
    }

    /**
     * Called when the user hits enter in the input text field.
     *
     * @param inputText the text the user had entered in the box when they hit enter
     */
    public void codeReceived(final String inputText)
    {
        checkCode(inputText.trim());
        statsWindow.rebuild(produce);
        current = getNextProduce();
        questionLabel.setText(current.name);
    }

    /**
     * Determines if the code was correct.
     * @param code the code the user entered
     */
    private void checkCode(final String code)
    {
        if (code.equals(current.number))
        {
            current.correct++;
            statsPanel.incrementCorrect();
            resultLabel.setForeground(Color.blue);
            resultLabel.setText("correct! " + current.name + " is " + current.number);
        }
        else
        {
            current.wrong++;
            statsPanel.incrementWrong();
            resultLabel.setForeground(Color.red);
            resultLabel.setText("wrong! " + current.name + ": " + current.number);
        }
    }

    public void showStatsWindow()
    {
        statsWindow.setVisible(true);
    }

    protected Produce getNextProduce()
    {
        if (produce.size() == 0)
        {
            return new Produce("Error: Missing or empty produce list file, " + PRODUCE_FILE, "");
        }
        return produce.get(rand());
    }

    private int rand()
    {
        return (int)(Math.random() * produce.size());
    }
}