package com.jordansterling.codelearner;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Jordan Sterling
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */l

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * A popup window that has detailed stats
 */
public class StatsWindow extends JFrame
{
    private final HashMap<Produce, JLabel> map;

    public StatsWindow(final CodeLearner otherWindow, final Vector<Produce> newItems)
    {
        super("Stats");
        map = new HashMap<Produce, JLabel>();
        add(new JScrollPane(buildLabels(newItems)));
        setSize(300, 400);
        setLocation(otherWindow.getX() + 400, otherWindow.getY());
    }

    public void rebuild(final Vector<Produce> newItems)
    {
        for (Produce newItem : newItems) {
            final JLabel label = map.get(newItem);
            final float total =  (float) newItem.correct + newItem.wrong;
            final double rate = total > 0 ? (newItem.correct / total) : 0.0;
            label.setText(String.format("%d    %d    %.1f%%", newItem.correct, newItem.wrong, rate * 100));
        }
    }

    private JPanel buildLabels(final Vector<Produce> newItems)
    {
        final JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridLayout(newItems.size(), 2));
        for (Produce newItem : newItems)
        {
            final JLabel dataLabel = new JLabel("-");
            labelPanel.add(new JLabel(newItem.name));
            labelPanel.add(dataLabel);
            map.put(newItem, dataLabel);
        }
        return labelPanel;
    }
}