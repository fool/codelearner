package com.jordansterling.codelearner;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Jordan Sterling
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import javax.swing.JTextField;
import java.awt.event.*;
import java.awt.Color;

/**
 * A customized JTextField that responds to the enter key event and notifies a CodePanel
 */
public class GroceryCodeTextField extends JTextField
{
    public GroceryCodeTextField(final CodePanel panel)
    {
        super(10);
        setCaretColor(Color.black);
        //addKeyListener(new MyKeyAdapter(panel, this));
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(final KeyEvent keyEvent) {
                super.keyPressed(keyEvent);
                if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    panel.codeReceived(getText());
                    reset();
                }
            }
        });
    }

    public void reset()
    {
        setText("");
        setCaretPosition(0);
    }
/*
    private class MyKeyAdapter extends KeyAdapter implements KeyListener
    {
        final private CodePanel panel;
        final private GroceryCodeTextField groceryCodeTextField;

        public MyKeyAdapter(final CodePanel aPanel, final GroceryCodeTextField field)
        {
            panel = aPanel;
            groceryCodeTextField = field;
        }

        public void keyPressed(final KeyEvent k)
        {

            {
                panel.codeReceived(groceryCodeTextField.getText());
                groceryCodeTextField.reset();
            }
        }
    }
    */
}