package com.jordansterling.codelearner;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Jordan Sterling
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Stats panel looks like
 *
 * Codes: 50      Correct: 35    Incorrect:  15       Rate:   75%        Last 20: 90%
 */
public class StatsPanel extends JPanel
{
    private int correct;
    private int wrong;
    private int codes;
    private int last20Index;

    final private JLabel correctLabel;
    final private JLabel wrongLabel;
    final private JLabel codesLabel;
    final private JLabel rateLabel;
    final private JLabel last20RateLabel;
    final private int[] last20;
    final private JPanel bottomPanel;

    public StatsPanel(final CodePanel panel)
    {
        correct = 0;
        wrong = 0;
        last20 = new int[20];
        last20Index = 0;

        // TODO: Font / color
        codesLabel = new JLabel("Codes: 0");
        correctLabel = new JLabel("Correct: 0");
        wrongLabel = new JLabel("Wrong: 0");
        rateLabel = new JLabel("Rate: 0%");
        last20RateLabel = new JLabel("Rate (Last 20): 0%");

        final JButton statsButton = new JButton("Stats");
        statsButton.setToolTipText("Show statistics on each individual PLU code");
        statsButton.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent actionEvent) {
                panel.showStatsWindow();
            }
        });
        final JButton helpButton = new JButton("?");
        helpButton.setToolTipText("Toggle help");
        helpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                toggleHelp();
            }
        });

        final JPanel topPanel = new JPanel();
        bottomPanel = new JPanel();
        setLayout(new BorderLayout());

        final String help =
            "The program has a built in list of codes, which are from 2007 and may be specific to the store I worked" +
            " at. You can provide your own list of PLU codes if there are differences in your store.\n\nMake a file" +
            " named \"codes.txt\"." +
            " It must be formatted with one PLU code per line. Each line begins with a code, followed by a space, followed" +
            "by the item name.\n\nRegular expression used to parse lines: " + CodePanel.CODE_REGEX + "" +
            "\n\nFor more information visit: https://bitbucket.org/fool/codelearner";
        final JTextArea helpText = new JTextArea(help, 4, 40);
        helpText.setLineWrap(true);
        helpText.setWrapStyleWord(true);
        helpText.setEditable(false);

        topPanel.add(codesLabel);
        topPanel.add(correctLabel);
        topPanel.add(wrongLabel);
        topPanel.add(rateLabel);
        topPanel.add(last20RateLabel);
        topPanel.add(statsButton);
        topPanel.add(helpButton);
        bottomPanel.add(new JScrollPane(helpText));
        bottomPanel.setVisible(false);

        add(topPanel, BorderLayout.NORTH);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    private void toggleHelp()
    {
        bottomPanel.setVisible(!bottomPanel.isVisible());
    }

    public void incrementCorrect()
    {
        correct++;
        correctLabel.setText("Correct: " + correct);
        last20[last20Index] = 1;
        incrementCodes();
    }

    public void incrementWrong()
    {
        wrong++;
        wrongLabel.setText("Wrong: " + wrong);
        last20[last20Index] = 0;
        incrementCodes();
    }

    protected void incrementCodes()
    {
        codes++;
        codesLabel.setText("Codes: " + codes);
        rateLabel.setText(String.format("Rate: %.2f%%", 100.0 * (correct / (double) codes)));
        int last20Rate = 0;
        for (int i = 0; i < codes && i < 20; i++) {
            last20Rate += last20[i];
        }
        last20RateLabel.setText(String.format("Rate (last %d): %.2f%%", Math.min(20, codes), 100.0 * (last20Rate / Math.min(20.0, (double) codes))));
        last20Index = (last20Index + 1) % 20;
    }
}
