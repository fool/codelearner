Code Learner
8/13/07 Jordan Sterling

This is GUI a program to teach you grocery PLU codes. I wrote this while working at a grocery store and had to pass the test.

There is a built in file of PLU codes but it may not be complete or accurate. You can override this default file by creating a file named `codes.txt` in the same folder as the jar.
